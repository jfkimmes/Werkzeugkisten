# Werkzeugkisten
A personal collection of usecase specific container toolboxes.

## Usage
Choose a set of toolboxes by editing `distrobox.ini`. It creates downloads and sets up the latest versions of the selected toolboxes.

Run the following command to create them:
```
distrobox assemble create
```

Then enter one of the selected toolboxes, e.g.:
```
distrobox enter general
```


# Development
## Manually build a container
E.g. for the 'fedora-tex' toolbox:
```
docker build -f ./tex/Containerfile -t fedora-tex .
distrobox create --image fedora-tex --name tex
```

## Add a new toolbox
- Add dir with `Containerfile` and all necessary files
- Add toolbox to `distrobox.ini`
- Add CI config `.woodpecker/`

